<?
require( 'include/mysql_class.php' );
require( 'include/utils.php' );
mysql_query( 'SET NAMES utf8' );
$company = getCompanyAlias();
$sqlCompany= " SELECT * FROM `mydigita_dbv1`.`COMPANY` WHERE `CompanyAlias`='$company'";

$lang = "es";
$micon->consulta($sqlCompany);
$dtaCoDetail  = $micon->campoconsultaA();
$coords = "https://www.google.com/maps/search/?api=1&query=" . $dtaCoDetail[ CompanyLat ] .",".$dtaCoDetail[ CompanyLog ]."&query_place_id=" . $dtaCoDetail[ CompanyMapPlaceID ];

$lang         = $dtaCoDetail[CompanyLang];
include("lang/".$lang.".php");

?>

<!DOCTYPE html>
<html lang="<?=$lang?>" >
	<head>
        
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100691322-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push( arguments );
        }
        gtag( 'js', new Date() );

        gtag( 'config', 'UA-100691322-2' );
    </script>
        
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title>  
            My Digital Card - <?=$dtaCoDetail[CompanyName] ?> </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="css2/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/fontawesome-all.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
        <meta name="theme-color" content="<?=$dtaCoDetail[CompanyColor]?>">

		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-skins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css2/card.css">
        <link rel="stylesheet" href="css/animate.css">
		<!-- SmartAdmin RTL Support -->
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-rtl.min.css"> 

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css2/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="//app.mydigital.cards/srvs/media/images/companies/<?=$dtaCoDetail[Companylogo] ?>" type="image/x-icon">
		<link rel="icon" href="//app.mydigital.cards/srvs/media/images/companies/<?=$dtaCoDetail[Companylogo] ?>" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700"> 

		<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="//app.mydigital.cards/srvs/media/images/companies/<?=$dtaCoDetail[Companylogo] ?>">
		<link rel="apple-touch-icon" sizes="76x76" href="//app.mydigital.cards/srvs/media/images/companies/<?=$dtaCoDetail[Companylogo] ?>">
		<link rel="apple-touch-icon" sizes="120x120" href="//app.mydigital.cards/srvs/media/images/companies/<?=$dtaCoDetail[Companylogo] ?>">
		<link rel="apple-touch-icon" sizes="152x152" href="//app.mydigital.cards/srvs/media/images/companies/<?=$dtaCoDetail[Companylogo] ?>">

	</head>
	
	
	
	<body class=" smart-style-6">

	
		<div id="main" role="main">

			<!-- RIBBON -->
			
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- Bread crumb is created dynamically -->
				<!-- row -->
				
				<!-- end row -->
				
				<!-- row -->
				
				
				
				
							
				
                      

                          

                                    <div class="row">

                                        <div class="col-sm-12 no-padding">
                                            <div id="myCarousel" class="carousel fade profile-carousel animated fadeIn">
                                              
                                                
                                                
                                                <div class="carousel-inner" style="background-color: <?=$dtaCoDetail[CompanyColor]?>;">
                                                     <div class=" profile-pic">
                                                    <img src="images/nophoto.jpg" class="animated bounceIn" style="animation-delay: 0.3s;">
                                                    <h1 style="animation-delay: 0.3s;color:<?=$dtaCoDetail[CompanyColorText]?>" class=" animated fadeInUp" >
                                                        <span  style="animation-delay: 1.2s">  <?=CONTACT_NOT_FOUND?> </span>
                                                    <br>
                                                    </h1>
                                                        
                                         <a href="javascript:void(0);" style="animation-delay: 1.4s;" class="btn btn-default  btn-xs animated zoomIn" rel="popover" data-placement="top" data-original-title="<i class='fas fa-map-marker-alt'></i>  <? echo ADDRESS . " " . $dtaCoDetail[CompanyExtra] ?>" data-content="<?=$dtaCoDetail[CompanyAddress] . ", ".$dtaCoDetail[CompanyCityContry]  ?>
 <a class='btn btn-default btn-block btn-xs' target='_blank' href='<?=$coords?>'>Ir</a>  " data-html="true"><i class='fas fa-map-marker-alt'></i>  <i><?=$dtaCoDetail[CompanyName]?></i></a> 
                                                </div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-4 col-md-4  col-lg-4 col-sm-offset-4 col-md-offset-4 col-xs-offset-0">

                                            <div class="padding-10">
                                                
                                                
                                                
                                                
                                                <? if($dtaCoDetail[CompanyPhone]!="") { ?>
                                                
                                                <a href="tel:<?=$dtaCoDetail[CompanyPhone] ?>" class="btn btn-labeled btn-default text-left btn-md btn-block animated fadeInUp" style="animation-delay: 1.4s;"> <span class="btn-label"><i class="fas fa-fax fa-2x" style="color:<?=$dtaCoDetail[CompanyColor]?>;"></i></span><div class="labelinfo"><?=$dtaCoDetail[CompanyPhone] ?></div><div class="text-muted font-xs littlelabel"><?=PHONE ?></div></a>
												<? } ?>
                                           
                                                
                                                <div class="text-center padding-10 animated fadeIn" style="   animation-delay: 2.6s;"> 
                                                    
                                                    <? if ($dtaCoDetail[CompanyWeb]!=""){  ?>
                                                   
                                                    
                                                    <a href="<?=$dtaCoDetail[CompanyWeb]?>" target="_blank" class="btn btn-default btn-lg"><i class="fas fa-link fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>

                                                    <? if ($dtaCoDetail[CompanyFB]!=""){  ?>
                                                    
                                                    
                                                    <a href="<?=$dtaCoDetail[CompanyFB]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-facebook fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>

                                                    <? if ($dtaCoDetail[CompanyTW]!=""){  ?>
                                                    
                                                    
                                                    
                                                    <a href="<?=$dtaCoDetail[CompanyTW]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-twitter fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>
                                                    <? if ($dtaCoDetail[CompanyIG]!=""){  ?>
                                                    
                                                    
                                                    
                                                    <a href="<?=$dtaCoDetail[CompanyIG]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-instagram fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>
                                                    <? if ($dtaCoDetail[CompanyIN]!=""){  ?>
                                                   
                                                    
                                                    
                                                    <a href="<?=$dtaCoDetail[CompanyIN]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-linkedin fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>
                                                    <? if ($dtaCoDetail[CompanyYT]!=""){  ?>
                                                    
                                                    
                                                    
                                                    <a href="<?=$dtaCoDetail[CompanyYT]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-youtube fa-lg text-muted"></i></a>
                                                    
                                                    
                                                    <? } ?>

                                                    <? if ($dtaCoDetail[CompanyGP]!=""){  ?>
                                                    
                                                    
                                                    
                                                     <a href="<?=$dtaCoDetail[CompanyGP]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-google-plus fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                     
                                                </div>
                                                
                                            </div>

                                        </div>

                                    </div>
                                         
               
                                    
                                    <!-- end row -->

                               


							
				
				
					
				
				<!-- end row -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4  col-lg-4 col-sm-offset-4 col-md-offset-4 col-xs-offset-0 text-center">
					<img src="images/<?=$company?>/logo.png" class="<?=$dtaCoDetail[CompanyLogoClass] ?> animated fadeInDown" style="   animation-delay: 2.8s;" > 
				</div>

				
			</div>
		</div>
			
		
		<!-- END PAGE FOOTER -->

		<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
		
		<!-- END SHORTCUT AREA -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js2/plugin/pace/pace.min.js"></script>

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="js2/libs/jquery-3.2.1.min.js"><\/script>');
			}
		</script>

		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="js2/libs/jquery-ui.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="js2/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="js2/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="js2/bootstrap/bootstrap.min.js"></script>






		<!-- browser msie issue fix -->
		<script src="js2/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="js2/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		

		<!-- MAIN APP JS FILE -->
		<script src="js2/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="js2/speech/voicecommand.min.js"></script>

		<!-- SmartChat UI : plugin -->
		<script src="js2/smart-chat-ui/smart.chat.ui.min.js"></script>
		<script src="js2/smart-chat-ui/smart.chat.manager.min.js"></script>

		<!-- PAGE RELATED PLUGIN(S) 
		<script src="..."></script>-->

		<script>
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
            
           
		
		})

		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		

	</body>

</html>