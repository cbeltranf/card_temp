<?
require( 'include/mysql_class.php' );
require( 'include/utils.php' );
mysql_query( 'SET NAMES utf8' );

$idUser =   $_GET[user];
$company = getCompanyAlias();

//print_r( $idUser);
$sqlContact = "SELECT  
    it_users.name AS UserName
    , it_users.last_name AS UserLastName
    , it_positions.name AS PositionName
    , it_users.phone AS UserMobile
    , it_users.country_phone AS UserCountryPhoneCode
    , it_users.login_alt AS UserLogin
    , it_users.email AS USER_Mail
    , it_business.name AS CompanyName
    , it_branches.address AS CompanyAddress
    , it_branches.phone AS CompanyPhone
    , it_branches.it_cities_id AS CompanyCityContry
    , it_branches.latitude AS CompanyLat
    , it_branches.longitude AS CompanyLog
    , it_users.avatar AS UserAvatar
    , it_branches.phone AS CompanyPhone
    , it_business.backgroundcolor AS CompanyColor
    , it_business.logo AS Companylogo
    , \"en\" AS CompanyLang
    , it_users.languaje AS UserLang
    , soc1.account AS CompanyFB
    , soc2.account AS CompanyTW
    , soc3.account AS CompanyYT
    , it_business.logo AS CompanyLogoClass
    , it_business.textcolor AS CompanyColorText
    
FROM
    vs_login AS it_users
    INNER JOIN it_positions 
        ON (it_users.it_positions_id = it_positions.id)
    INNER JOIN it_branches
        ON (it_branches.id = it_users.it_branches_id)
    INNER JOIN it_business
        ON (it_business.id = it_branches.`it_business_id`)
    LEFT JOIN `it_branch_social_network` AS soc1 
	ON (soc1.`it_branches_id` = it_branches.`id`)
    LEFT JOIN `it_branch_social_network` AS soc2 
	ON (soc2.`it_branches_id` = it_branches.`id`)
    LEFT JOIN `it_branch_social_network` AS soc3 
	ON (soc3.`it_branches_id` = it_branches.`id`)
       WHERE it_users.id='$idUser' AND it_users.status='A' AND soc1.`it_social_networks_id` = 1 AND 
			soc2.`it_social_networks_id` = 3 AND soc3.`it_social_networks_id` = 2";  

/*
$sqlContact = " SELECT  
    `it_users`.`name` as UserName
    , `it_users`.`last_name` as UserLastName
    , `it_positions`.`name`PositionName
    , `it_users`.`UserMobile`
    , `it_users`.`UserCountryPhoneCode`
     , `UserLogin` 
    , `usercompany`.`USER_Mail`
    , `company`.`CompanyName`
    , `CompanyAddress`
    , `CompanyPhone`
    , `CompanyCityContry`
    , `CompanyWeb`
    , `CompanyLat`
    , `CompanyLog`
    , `UserAvatar`
    , `CompanyPhone`
    , `CompanyColor`
    , CompanyExtra
    , Companylogo
    , CompanyLang
    , UserLang
    , CompanyFB
    ,CompanyIG
    ,CompanyIN
    ,CompanyTW
    ,CompanyYT
    ,CompanyGP
    ,CompanyLogoClass
    ,CompanyColorText
    , CompanyMapPlaceID
    
FROM
    `usercompany`
    INNER JOIN `it_users` 
        ON (`usercompany`.`USER_UserId` = `user`.`UserId`)
    INNER JOIN `it_positions` 
        ON (`usercompany`.`POSITION_PositionId` = `it_positions`.`PositionId`)
    INNER JOIN `company` 
        ON (`usercompany`.`EMPRESA_EmpresaId` = `company`.`CompanyId`)
       WHERE `CompanyAlias`='$company' AND `UserId`='$idUser' AND `UserStatus`='A'";
*/


//echo $sqlContact;

//exit();
$micon->consulta( $sqlContact );
$dtaNumReg = $micon->numregistros();

//exit();

$lang = "es";
if ( $dtaNumReg == 0 ) {
	//if ($company!="claro")
   header( 'Location: error.php' );
   exit();
} else {
    $dtaContactDetail = $micon->campoconsultaA();
    //FULL NAME FIXED
    $lang = $dtaContactDetail[ UserLang ];
    if ($lang==""){
    	$lang = "en";
    }
    require( "lang/" . $lang . ".php" );

    $contactName =  $dtaContactDetail[ UserName ];
    $contactName =  $dtaContactDetail[ UserName ];
   // $contactName = mb_convert_case( $dtaContactDetail[ UserName ], MB_CASE_TITLE, "UTF-8" );
    $contactLastName = mb_convert_case( $dtaContactDetail[ UserLastName ], MB_CASE_TITLE, "UTF-8" );;
    $fullName = $contactName . " " . $contactLastName;

    //position FIXED
    $contactPositionCapitalize = mb_convert_case( $dtaContactDetail[ PositionName ], MB_CASE_TITLE, "UTF-8" );
    $contactPositionArr = explode( " ", $contactPositionCapitalize );
    $firstWordPositon = $contactPositionArr[ 0 ];
    if ( count( $contactPositionArr ) > 1 ) {
        array_shift( $contactPositionArr );
        $lastWordPositon = join( " ", $contactPositionArr );
    }

    //CONTACT
    $contactPhone = $dtaContactDetail[ UserMobile ];
    $contactEmail = $dtaContactDetail[ USER_Mail ];
    $contactLocation = $dtaContactDetail[ CompanyCityContry ]; //Reemplazar por "card_city_country"
    $contactAddress = $dtaContactDetail[ CompanyAddress ]; //Reemplazar por "card_address"

    $coords = "https://www.google.com/maps/search/?api=1&query=" . $dtaContactDetail[ CompanyLat ] .",".$dtaContactDetail[ CompanyLog ]."&query_place_id=" . $dtaContactDetail[ CompanyMapPlaceID ];
 
    //IMG
    if ( $dtaContactDetail[ UserAvatar ] != "nouser.png" ) {
        //$photoName = $dtaContactDetail[UserAvatar];		

        $photoName = SRVS2 . $dtaContactDetail[ UserAvatar ];


    } else {
        $photoName = "images/nophoto.jpg";
    }


    $imageBase64 = "";

    if ( $photoName != "images/nophoto.jpg" ) {
        $imageString = file_get_contents(SRVS . $dtaContactDetail[ UserAvatar ] );
        
        $imageBase64 = "PHOTO;ENCODING=b;TYPE=JPEG:" . base64_encode( $imageString );

    }

    //CREATE VFCARD
    $vcfFile = fopen( "vcard/" . $dtaContactDetail[ UserLogin ] . ".vcf", "w" )or die( "Unable to open file!" );

    $vcfContent = "BEGIN:VCARD
VERSION:3.0
N:$contactLastName;$contactName;;
FN:$contactName $contactLastName
EMAIL;TYPE=WORK:$contactEmail
ORG:$dtaContactDetail[CompanyName]
TEL;TYPE=WORK:$dtaContactDetail[UserCountryPhoneCode]$contactPhone
TITLE:$contactPositionCapitalize
URL;WORK:$dtaContactDetail[CompanyWeb]
NOTE:http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]
$imageBase64
END:VCARD";
    fwrite( $vcfFile, $vcfContent );
    fclose( $vcfFile );

}

?>

<!DOCTYPE html>
<html lang="<?=$lang?>" >
	<head>
        
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100691322-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push( arguments );
        }
        gtag( 'js', new Date() );

        gtag( 'config', 'UA-100691322-2' );
    </script>
        
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title>  
            <?=CONTACT ?>:
        <?=$fullName ?> -
        <?=$dtaContactDetail[CompanyName] ?> </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="css2/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/fontawesome-all.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
        <meta name="theme-color" content="<?=$dtaContactDetail[CompanyColor]?>">

		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-skins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css2/card.css">
        <link rel="stylesheet" href="css2/animate.css">
		<!-- SmartAdmin RTL Support -->
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-rtl.min.css"> 

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css2/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?=SRVS . $dtaContactDetail[Companylogo] ?>" type="image/x-icon">
		<link rel="icon" href="<?=SRVS . $dtaContactDetail[Companylogo] ?>" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700"> 

		<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="<?=SRVS . $dtaContactDetail[Companylogo] ?>">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=SRVS . $dtaContactDetail[Companylogo] ?>">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=SRVS . $dtaContactDetail[Companylogo] ?>">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=SRVS . $dtaContactDetail[Companylogo] ?>">

	</head>
	
	
	
	<body class=" smart-style-6">

	
		<div id="main" role="main">

			<!-- RIBBON -->
			
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- Bread crumb is created dynamically -->
				<!-- row -->
				
				<!-- end row -->
				
				<!-- row -->
				
				
				
				
							
				
                      

                          

                                    <div class="row">

                                        <div class="col-sm-12 no-padding">
                                            <div id="myCarousel" class="carousel fade profile-carousel animated fadeIn">
                                              
                                                
                                                
                                                <div class="carousel-inner" style="background-color: <?=$dtaContactDetail[CompanyColor]?>;">
                                                     <div class=" profile-pic">
													<?
													if($company!='claro'){	 
													?>	 
                                                    <img src="<?=$photoName?>" class="animated bounceIn" style="animation-delay: 0.3s;width: 100px;height: 100px;  object-fit: cover;">
													<? } ?>	 
                                                    <h1 style="animation-delay: 0.3s;color:<?=$dtaContactDetail[CompanyColorText]?>" class=" animated fadeInUp" >
                                                        <span  style="animation-delay: 1.2s">    <?=$contactName?> <span class="semi-bold"><?=$contactLastName?></span> </span>
														
                                                    <br>
                                                    <small class="animated fadeInDown" style="animation-delay: 0.8s;color: <?=$dtaContactDetail[CompanyColorText]?>"> <?=$contactPositionCapitalize  ?></small>
														 
														 </h1>
                                           <?
													if($company!='claro'){	 
													?>             
                                         <a href="javascript:void(0);" style="animation-delay: 1.4s;" class="btn btn-default  btn-xs animated zoomIn" rel="popover" data-placement="top" data-original-title="<i class='fas fa-map-marker-alt'></i>  <? echo ADDRESS . " " . $dtaContactDetail[CompanyExtra] ?>" data-content="<?=$contactAddress . ", ".$dtaContactDetail[CompanyCityContry]  ?>
 <a class='btn btn-default btn-block btn-xs' target='_blank' href='<?=$coords?>'><?=GO?></a>  " data-html="true"><i class='fas fa-map-marker-alt'></i>  <i><?=$dtaContactDetail[CompanyName]?></i></a> 
														<? } ?> 
                                                </div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-4 col-md-4  col-lg-4 col-sm-offset-4 col-md-offset-4 col-xs-offset-0">

                                            <div class="padding-10">
                                                
                                                
                                                
                                                <a href="tel:<?=$contactPhone?>" class="btn btn-labeled btn-default text-left btn-md btn-block animated fadeInUp" style="animation-delay: 1.0s;"> <span class="btn-label"><i class="fas fa-phone fa-2x" style="color:<?=$dtaContactDetail[CompanyColor]?>;"></i></span><div class="labelinfo"> <?=$contactPhone?></div><div class="text-muted font-xs littlelabel"><?=MOBILE ?></div></a>
                                                
												
                                                <a href="mailto:<?=$contactEmail?>" class="btn btn-labeled btn-default text-left btn-md btn-block animated fadeInUp" style="animation-delay: 1.2s;"> <span class="btn-label"><i class="fas fa-envelope fa-2x" style="color:<?=$dtaContactDetail[CompanyColor]?>;"></i></span><div class="labelinfo"><?=$contactEmail?></div><div class="text-muted font-xs littlelabel"><?=EMAIL ?></div></a>
                                                
												
                                                <? if($dtaContactDetail[CompanyPhone]!="") { ?>
                                                <a href="tel:<?=$dtaContactDetail[CompanyPhone] ?>" class="btn btn-labeled btn-default text-left btn-md btn-block animated fadeInUp" style="animation-delay: 1.4s;"> <span class="btn-label"><i class="fas fa-fax fa-2x" style="color:<?=$dtaContactDetail[CompanyColor]?>;"></i></span><div class="labelinfo"><?=$dtaContactDetail[CompanyPhone] ?></div><div class="text-muted font-xs littlelabel"><?=PHONE ?></div></a>
												<? } ?>
                                                
                                               <button name="btnSaveContact" value="<?=$dtaContactDetail[UserLogin]."-" . $company ?>" type="button" id="btnSaveContact" class="btn btn-default btn-lg btn-block animated fadeInDown" style="animation-delay: 1.8s;color:<?=$dtaContactDetail[CompanyColorText]?>;background-color: <?=$dtaContactDetail[CompanyColor]?>; font-weight: bold; border-radius: 3px" >
								<?=SAVE_CONTACT ?>
							</button>
                                                
                                                <div class="text-center padding-10 animated fadeIn" style="   animation-delay: 2.6s;"> 
                                                    
                                                    <? if ($dtaContactDetail[CompanyWeb]!=""){  ?>
                                                   
                                                    
                                                    <a href="<?=$dtaContactDetail[CompanyWeb]?>" target="_blank" class="btn btn-default btn-lg"><i class="fas fa-link fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>

                                                    <? if ($dtaContactDetail[CompanyFB]!=""){  ?>
                                                    
                                                    
                                                    <a href="<?=$dtaContactDetail[CompanyFB]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-facebook fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>

                                                    <? if ($dtaContactDetail[CompanyTW]!=""){  ?>
                                                    
                                                    
                                                    
                                                    <a href="<?=$dtaContactDetail[CompanyTW]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-twitter fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>
                                                    <? if ($dtaContactDetail[CompanyIG]!=""){  ?>
                                                    
                                                    
                                                    
                                                    <a href="<?=$dtaContactDetail[CompanyIG]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-instagram fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>
                                                    <? if ($dtaContactDetail[CompanyIN]!=""){  ?>
                                                   
                                                    
                                                    
                                                    <a href="<?=$dtaContactDetail[CompanyIN]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-linkedin fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?>
                                                    <? if ($dtaContactDetail[CompanyYT]!=""){  ?>
                                                    
                                                    
                                                    
                                                    <a href="<?=$dtaContactDetail[CompanyYT]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-youtube fa-lg text-muted"></i></a>
                                                    
                                                    
                                                    <? } ?>

                                                    <? if ($dtaContactDetail[CompanyGP]!=""){  ?>
                                                    
                                                    
                                                    
                                                     <a href="<?=$dtaContactDetail[CompanyGP]?>" target="_blank" class="btn btn-default btn-lg"><i class="fab fa-google-plus fa-lg text-muted"></i></a>
                                                    
                                                    <? } ?> 
													                                                   
                                                </div>
                                                
                                            </div>

                                        </div>

                                    </div>
                                         
               
                                    
                                    <!-- end row -->

                               


							
				
				
					
				
				<!-- end row -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		 <div class="page-footer">
			<div class="row"> 
				<div class="col-xs-12 col-sm-4 col-md-4  col-lg-4 col-sm-offset-4 col-md-offset-4 col-xs-offset-0 text-center">
					<img src="<?=SRVS . $dtaContactDetail[Companylogo] ?>" class="center-block  animated fadeInDown img-responsive" style="   animation-delay: 2.8s; max-height: 90px;" > 
				</div>

				
			</div>
		</div> 
			
		
		<!-- END PAGE FOOTER -->

		<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
		
		<!-- END SHORTCUT AREA -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js2/plugin/pace/pace.min.js"></script>

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="js2/libs/jquery-3.2.1.min.js"><\/script>');
			}
		</script>

		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="js2/libs/jquery-ui.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="js2/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="js2/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="js2/bootstrap/bootstrap.min.js"></script>






		<!-- browser msie issue fix -->
		<script src="js2/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="js2/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		

		<!-- MAIN APP JS FILE -->
		<script src="js2/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->


		<!-- PAGE RELATED PLUGIN(S) 
		<script src="..."></script>-->

		<script>
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
            
            $('#btnSaveContact').click(function () {
            var card = $(this).val();
                window.location.href = 'vcard/' + card + '.vcf';
            });


            setTimeout(function(){ 
                $('#btnSaveContact').removeClass('fadeInDown ').addClass('shake'); 
            }, 3000);
		
		})

		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		

	</body>

</html>