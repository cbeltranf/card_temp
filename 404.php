<?
require( 'srvs/include/mysql_class.php' );
require( 'srvs/include/utils.php' );
mysql_query( 'SET NAMES utf8' );


?>

<!DOCTYPE html>
<html lang="<?=$lang?>" >
	<head>
        
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100691322-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push( arguments );
        }
        gtag( 'js', new Date() );

        gtag( 'config', 'UA-100691322-2' );
    </script>
        
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title>  
            My Digital Card - Not Found </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="css2/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/fontawesome-all.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
        <meta name="theme-color" content="#000">

		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-skins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css2/card.css">
        <link rel="stylesheet" href="css/animate.css">
		<!-- SmartAdmin RTL Support -->
		<link rel="stylesheet" type="text/css" media="screen" href="css2/smartadmin-rtl.min.css"> 

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css2/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		

		<!-- FAVICONS -->
		

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700"> 

		

	</head>
	
	
	
	<body class=" smart-style-6">

	
		<div id="main" role="main">

			<!-- RIBBON -->
			
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- Bread crumb is created dynamically -->
				<!-- row -->
				
				<!-- end row -->
				
				<!-- row -->
				
				
				
				
							
				
                      

                          

                                    <div class="row">

                                        <div class="col-sm-12 no-padding">
                                            <div id="myCarousel" class="carousel fade profile-carousel animated fadeIn">
                                              
                                                
                                                
                                                <div class="carousel-inner" style="background-color: <?=$dtaCoDetail[CompanyColor]?>;">
                                                     <div class=" profile-pic">
                                                   <i class='fas fa-exclamation-triangle' style="    font-size: 100px;"></i>
                                                    <h1 style="animation-delay: 0.3s;color:<?=$dtaCoDetail[CompanyColorText]?>;    font-size: xx-large;" class=" animated fadeInUp" >
                                                        <span  style="animation-delay: 1.2s">  NOT FOUND </span>
                                                    
                                                    </h1>
                                                        
                                         
                                                </div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        

                                    </div>
                                         
               
                                    
                                    <!-- end row -->

                               


							
				
				
					
				
				<!-- end row -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		
			
		
		<!-- END PAGE FOOTER -->

		<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
		
		<!-- END SHORTCUT AREA -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js2/plugin/pace/pace.min.js"></script>

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="js2/libs/jquery-3.2.1.min.js"><\/script>');
			}
		</script>

		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="js2/libs/jquery-ui.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="js2/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="js2/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="js2/bootstrap/bootstrap.min.js"></script>






		<!-- browser msie issue fix -->
		<script src="js2/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="js2/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		

		<!-- MAIN APP JS FILE -->
		<script src="js2/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="js2/speech/voicecommand.min.js"></script>

		<!-- SmartChat UI : plugin -->
		<script src="js2/smart-chat-ui/smart.chat.ui.min.js"></script>
		<script src="js2/smart-chat-ui/smart.chat.manager.min.js"></script>

		<!-- PAGE RELATED PLUGIN(S) 
		<script src="..."></script>-->

		<script>
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
            
           
		
		})

		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		

	</body>

</html>