<?
function friendlyString($string){
    $string = str_replace(array('[', ']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\1', $string );
    //$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , ' ', $string);
    return trim($string);
}
function friendlyDate( $date, $lDay, $lMonth, $dispHour,$global ) {
    //echo $lDay;
    //print_r( $global->arrayLongDays );
    $arrayDays = array(); 
    $arrayMonth = array();
    
	if ( $lDay ) {
         $arrayDays = $global->arrayLongDays;
        
        //$arrayDays 
        // print_r( $arrayDays );
 
	} else {
        
		
         $arrayDays = $global->arrayShortDays;

        
	}
	if ( $lMonth ) {
		
        $arrayMonth = $global->arrayLongMonth;
       // print_r( $arrayMonth );
        
	} else {
		
        $arrayMonth = $global->arrayShortMonth;
       // print_r( $arrayMonth );
        
        
	}
	$hourFormatted = "";
	if ( $dispHour ) {
		$hourFormatted = " | " . date( 'H:i', strtotime( $date ) );
	}

	$day = $arrayDays[ date( "N", strtotime( $date ) ) - 1 ];
	$month = $arrayMonth[ date( "n", strtotime( $date ) ) - 1 ];
    
	
    $dateUnion = ($global->of!="" ) ?  $global->of :  date( 'S', strtotime( $date ) ) ." ";
   
     $friendlyFormat = $day . " " . date( 'd', strtotime( $date ) ) .  $dateUnion . $month . $hourFormatted;

    

	return $friendlyFormat;
}






// return the interpolated value between pBegin and pEnd
function interpolate( $pBegin, $pEnd, $pStep, $pMax ) {
	if ( $pBegin < $pEnd ) {
		return ( ( $pEnd - $pBegin ) * ( $pStep / $pMax ) ) + $pBegin;
	} else {
		return ( ( $pBegin - $pEnd ) * ( 1 - ( $pStep / $pMax ) ) ) + $pEnd;
	}
}

// generate gradient swathe now

////ee3524 - c41230
function colorScale( $theColorBegin, $theColorEnd, $theNumSteps ) {
	$ArrColor = array();
	$theColorBegin = hexdec( $theColorBegin );
	$theColorEnd = hexdec( $theColorEnd );
	$theNumSteps = $theNumSteps - 1;

	$theR0 = ( $theColorBegin & 0xff0000 ) >> 16;
	$theG0 = ( $theColorBegin & 0x00ff00 ) >> 8;
	$theB0 = ( $theColorBegin & 0x0000ff ) >> 0;

	$theR1 = ( $theColorEnd & 0xff0000 ) >> 16;
	$theG1 = ( $theColorEnd & 0x00ff00 ) >> 8;
	$theB1 = ( $theColorEnd & 0x0000ff ) >> 0;


	for ( $i = 0; $i <= $theNumSteps; $i++ ) {
		$theR = interpolate( $theR0, $theR1, $i, $theNumSteps );
		$theG = interpolate( $theG0, $theG1, $i, $theNumSteps );
		$theB = interpolate( $theB0, $theB1, $i, $theNumSteps );

		$theVal = ( ( ( $theR << 8 ) | $theG ) << 8 ) | $theB;
		$hexColor = sprintf( "%06X", $theVal );
		// echo $theVal  . "-" ;
		array_push( $ArrColor, "#" . $hexColor );
		//$ArrColor $theTDARTag . "-";

	}
	return $ArrColor;
}
//print_r(colorScale("ee3524","c41230",1))  
function diffBetweenTimes($inDate1, $inDate2){
	$date1 = new DateTime( $inDate1 );
	$date2 = new DateTime( $inDate2 );

	$diff = $date2->diff($date1);

	$dayLabel = "";
	$hasDays = $diff->format('%a');
	if ($hasDays > 1){
	  $dayLabel = " days ";
	}
	elseif($hasDays=="1"){
		$dayLabel = "day ";
	}
	$timeElapsed = ($dayLabel!="") ? $hasDays . " " . $dayLabel : "";
	$timeElapsed .= $diff->format('%H:%I:%S');
	
	return $timeElapsed;
}

 function time_ago($timestamp, $global  )  {  
      $time_ago = strtotime($timestamp);  
      $current_time = time();  
      $time_difference = $current_time - $time_ago;  
      $seconds = $time_difference;  
      $minutes      = round($seconds / 60 );           // value 60 is seconds  
      $hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
      $days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
      $weeks          = round($seconds / 604800);          // 7*24*60*60;  
      $months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
      $years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
      if($seconds <= 60)  
      {  
     return $global->JUST_NOW;  
   }  
      else if($minutes <=60)  
      {  
     if($minutes==1)  
           {  
       return $global->ONE_MINUTE_AGO;  
     }  
     else  
           {  
       return $global->AGO . " $minutes " . $global->MINUTES_AGO;  
     }  
   }  
      else if($hours <=24)  
      {  
     if($hours==1)  
           {  
       return $global->AN_HOUR_AGO;  
     }  
           else  
           {  
       return $global->AGO ." $hours " . $global->HOURS_AGO;  
     }  
   }  
      else if($days <= 7)  
      {  
     if($days==1)  
           {  
       return $global->YESTERDAY ;  
     }  
           else  
           {  
       return $global->AGO . " $days " . $global->DAYS_AGO;  
     }  
   }  
      else if($weeks <= 4.3) //4.3 == 52/12  
      {  
     if($weeks==1)  
           {  
       return $global->A_WEEK_AGO ;  
     }  
     else  
    {  
        if ($weeks<3) {
            return $global->AGO  . " $weeks " . $global->WEEKS_AGO	 ;  
        }else{
            return friendlyDate($timestamp,true,false,false,$global);
        }
     }  
   }  
       else if($months <=12)  
      {
           
           return friendlyDate($timestamp,true,false,false,$global);
     /*if($months==1)  
           {  
       return $global->A_MONTH_AGO;  
     }  
           else  
           {  
       return $global->AGO  . " $months " . $global->MONTHS_AGO;  
     } */ 
   }  
      else  
      {  
          return friendlyDate($timestamp,true,false,false,$global);
     /* if($years==1)  
           {  
       return $global->ONE_YEAR_AGO;  
     }  
           else  
           {  
       return $global->AGO . " $years " . $global->YEARS_AGO ;  
     }  */
   }  
 }


function make_bitly_url($url){
  $bitly = 'http://api.bit.ly/shorten?version=2.0.1&amp;longUrl='.urlencode($url).'&amp;login=inmovsas&amp;apiKey=R_8eb5cc2d6d174a41bcb45eef81a3ab4a&amp;format=json';
    
  $response = file_get_contents($bitly);

	$json = json_decode($response,true);
	return $json['results'][$url]['shortUrl'];
}

function getCompanyAlias(){
    $url = "http://".$_SERVER['HTTP_HOST'];
    $parsedUrl = parse_url($url);
    $host = explode('.', $parsedUrl['host']);
    $subdomain = $host[0];
    $hostFull = array_shift($host);
    $hostFull = join('.', $host);

    return $subdomain;
}
?>