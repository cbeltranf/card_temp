<?
$global;
function setLang($lang){
    $global	= new stdClass();
  /*
        "textYourInfo" 	=>  $global->YOUR_INFO,
        "textCompanyInfo" 	=>  $global->COMPANY_INFO,
        "textMail" 	    =>  $global->MAIL,
        "textPosition"  =>  $global->POSITION,
        "textAddress"   =>  $global->ADDRESS,
        "textPhone"     =>  $global->LANDPHONE,
        "linkEdit"      =>  $global->EDIT,
        "linkDefault"   =>  $global->SETDEFAULT,
  */
    switch ($lang) {
        case "es":
        default:
             
             $global->arrayLongDays = array( "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo" );
             $global->arrayShortDays = array( "Lun.", "Mar.", "Mie.", "Jue.", "Vie.", "Sab.", "Dom." );
             $global->arrayLongMonth    = array( "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" );
             $global->arrayShortMonth   = array( "Ene.", "Feb.", "Mar.", "Abr.", "May.", "Jun.", "Jul.", "Ago.", "Sep.", "Oct.", "Nov.", "Dic." );
            $global->of  =  " de ";
            
           
            
            
            $global->AGO            =  "Hace";
            $global->JUST_NOW       =  "Justo ahora";
            $global->ONE_MINUTE_AGO =  "Hace un momento";
            $global->MINUTES_AGO    =  " minutos";
            $global->AN_HOUR_AGO    =  "Hace una hora";
            $global->HOURS_AGO      =  " horas";
            $global->YESTERDAY      =  "Ayer";
            $global->DAYS_AGO       =  " días";
            $global->A_WEEK_AGO     =  "Hace una semana";
            $global->WEEKS_AGO      =  " semanas";
            $global->A_MONTH_AGO    =  "Hace un mes";
            $global->MONTHS_AGO     =  " meses";
            $global->ONE_YEAR_AGO   =  "Hace un año";
            $global->YEARS_AGO      =  " años";
            
            $global->I_AM_SHARING_CARD   =  "Hola, te comparto mi tarjeta de contacto";
            $global->GREET_MSG      =  "atte.";
            $global->SENT_CARD      =  "Enviado";
            
            
            
             //ELIMINAR AL ACTUALIZAR
            $global->NEWS_READ_MORE = "Ver más...";   
            $global->LIKE  =  "Me gusta";
            $global->COMMENTS  =  "Comentarios";
            $global->CONGRATULATE  =  "Felicitar";
            $global->NO_POSTS  =  "No hay publicaciones, sé el primero en subir uno.";
            $global->NO_COMMENTS  =  "No hay comentarios para esta publicación, sé el primero en escribir uno.";
            
            
            
            $global->MAIL  =  "Correo";
            $global->POSITION  =  "Cargo";
            $global->COMPANY_INFO  =  "Datos de la empresa";
            $global->YOUR_INFO  =  "Tus datos";
            $global->ADDRESS  =  "Dirección";
            $global->LANDPHONE  =  "Teléfono";
            $global->EDIT  =  "Editar";
            $global->SETDEFAULT  =  "Predeterminar";
            
            
        break;
        case "en":        
            
            $global->arrayLongDays = array( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" );
            $global->arrayShortDays = array( "Mon.", "Tue.", "Wed.", "Thu.", "Fri.", "Sat.", "Sun." );
            $global->arrayLongMonth    = array( "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" );
             $global->arrayShortMonth  =  array( "Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec." );
             $global->of  =  "" ;
            
            
           
            
            
            $global->AGO            =  "";
            $global->JUST_NOW       =  "Just Now";
            $global->ONE_MINUTE_AGO =  "One minute ago";
            $global->MINUTES_AGO    =  " minutes ago";
            $global->AN_HOUR_AGO    =  "An hour ago";
            $global->HOURS_AGO      =  " hours ago";
            $global->YESTERDAY      =  "Yesterday";
            $global->DAYS_AGO       =  " days ago";
            $global->A_WEEK_AGO     =  "A week ago";
            $global->WEEKS_AGO      =  " weeks ago";
            $global->A_MONTH_AGO    =  "A month ago";
            $global->MONTHS_AGO     =  " months ago";
            $global->ONE_YEAR_AGO   =  "One year ago";
            $global->YEARS_AGO      =  " years ago";
            
            $global->I_AM_SHARING_CARD   =  "Hello, I am sharing my business card";
            $global->GREET_MSG      =  "regards";
            $global->SENT_CARD      =  "Sent";
            
             //ELIMINAR AL ACTUALIZAR
            $global->NEWS_READ_MORE = "Read More...";
            $global->LIKE  =  "Like";
            $global->COMMENTS  =  "Comments";
            $global->CONGRATULATE  =  "Congratulate";
            $global->NO_POSTS  =  "There are no Post, Be the first to write one ";
            $global->NO_COMMENTS  =  "No comments for this post, be the first to write one.";
            
            
            $global->MAIL  =  "E-mail";
            $global->POSITION  =  "Position";
            $global->COMPANY_INFO  =  "Company Info";
            $global->YOUR_INFO  =  "Your Information";
            $global->ADDRESS  =  "Address";
            $global->LANDPHONE  =  "Phone";
            $global->EDIT  =  "Edit";
            $global->SETDEFAULT  =  "Set as default";
            
         break;


    }
  //  echo $global->NEWS_READ_MORE;
   return  $global ;
}
?>